// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"fmt"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/bichon/source"
)

type IndexPage struct {
	Source    source.Source
	Messages  Messages
	Layout    *tview.Frame
	MergeReqs *tview.Table

	MergeReqsModel []model.MergeReq
}

func NewIndexPage(source source.Source, msgs Messages) *IndexPage {
	mreqs := tview.NewTable().
		SetSelectable(true, false).
		SetSelectedStyle(tcell.ColorYellow, tcell.ColorRed, tcell.AttrBold)

	layout := tview.NewFrame(mreqs).
		SetBorders(0, 0, 0, 0, 0, 0)

	page := &IndexPage{
		Source:    source,
		Messages:  msgs,
		Layout:    layout,
		MergeReqs: mreqs,
	}

	return page
}

func (page *IndexPage) GetName() string {
	return "index"
}

func (page *IndexPage) GetKeyShortcuts() string {
	return "q:Quit r:Refresh ?:Help"
}

func (page *IndexPage) updateMergeReqsJob(app *tview.Application) {
	mreqs, err := page.Source.GetMergeRequests()
	if err != nil {
		return
	}

	cells := make([][6]*tview.TableCell, 0)
	for _, mreq := range mreqs {
		vers, err := page.Source.GetVersions(&mreq)
		if err != nil {
			return
		}

		go func() {
			app.QueueUpdateDraw(func() {
				page.Messages.Info(fmt.Sprintf("Loading merge request %d patches...", mreq.ID))
			})
		}()
		patches, err := page.Source.GetPatches(&mreq, &vers[0])
		if err != nil {
			return
		}

		cells = append(cells, [6]*tview.TableCell{
			&tview.TableCell{
				Text:          fmt.Sprintf("#%-4d", mreq.ID),
				Color:         tcell.ColorWhite,
				Align:         tview.AlignRight,
				NotSelectable: false,
			},
			&tview.TableCell{
				Text:          mreq.Age(),
				Color:         tcell.ColorWhite,
				Align:         tview.AlignLeft,
				NotSelectable: false,
			},
			&tview.TableCell{
				Text:          mreq.Submitter.RealName,
				Color:         tcell.ColorWhite,
				Align:         tview.AlignLeft,
				NotSelectable: false,
			},
			&tview.TableCell{
				Text:          fmt.Sprintf("v%d", len(vers)),
				Color:         tcell.ColorWhite,
				Align:         tview.AlignLeft,
				NotSelectable: false,
			},
			&tview.TableCell{
				Text:          fmt.Sprintf("0/%d", len(patches)),
				Color:         tcell.ColorWhite,
				Align:         tview.AlignLeft,
				NotSelectable: false,
			},
			&tview.TableCell{
				Text:          mreq.Title,
				Color:         tcell.ColorWhite,
				Align:         tview.AlignLeft,
				NotSelectable: false,
			},
		})
	}

	go func() {
		app.QueueUpdateDraw(func() {
			page.MergeReqsModel = mreqs
			page.updateMergeReqs(cells)
			page.Messages.Info("")
		})
	}()
}

func (page *IndexPage) Refresh(app *tview.Application) {
	page.Messages.Info("Loading merge requests...")
	go page.updateMergeReqsJob(app)
}

func (page *IndexPage) SetFocus(app *tview.Application) {
	app.SetFocus(page.MergeReqs)
}

func (page *IndexPage) Navigate(event *tcell.EventKey) {
}

func (page *IndexPage) GetSelectedMergeReq() *model.MergeReq {
	row, _ := page.MergeReqs.GetSelection()

	if row >= len(page.MergeReqsModel) {
		return nil
	} else {
		return &page.MergeReqsModel[row]
	}
}

func (page *IndexPage) updateMergeReqs(cells [][6]*tview.TableCell) {
	page.MergeReqs.Clear()
	for idx, row := range cells {
		page.MergeReqs.SetCell(idx, 0, row[0])
		page.MergeReqs.SetCell(idx, 1, row[1])
		page.MergeReqs.SetCell(idx, 2, row[2])
		page.MergeReqs.SetCell(idx, 3, row[3])
		page.MergeReqs.SetCell(idx, 4, row[4])
		page.MergeReqs.SetCell(idx, 5, row[5])
	}
}
