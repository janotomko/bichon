// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package source

import (
	"gitlab.com/bichon-project/bichon/model"
)

type Source interface {
	Ping() error

	GetMergeRequests() ([]model.MergeReq, error)

	GetVersions(mreq *model.MergeReq) ([]model.Series, error)

	GetPatches(mreq *model.MergeReq, series *model.Series) ([]model.Commit, error)

	GetMergeRequestComments(mreq *model.MergeReq) ([]model.Comment, error)
}
