// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

type Page interface {
	GetName() string
	GetKeyShortcuts() string
	Navigate(event *tcell.EventKey)
	SetFocus(app *tview.Application)
}
