// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"fmt"
	"strings"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/bichon/source"
)

type App struct {
	Source       source.Source
	App          *tview.Application
	Layout       *tview.Flex
	KeyShortcuts *tview.TextView
	Status       *tview.TextView
	Messages     *MessageBar
	Pages        *tview.Pages
	Index        *IndexPage
	Detail       *DetailPage
	ThisPage     Page
}

func NewApp(repo *model.Repo, source source.Source) *App {
	msgs := NewMessageBar()
	app := &App{
		Source:       source,
		App:          tview.NewApplication(),
		Layout:       tview.NewFlex(),
		KeyShortcuts: tview.NewTextView().SetDynamicColors(true),
		Status:       tview.NewTextView().SetDynamicColors(true),
		Messages:     msgs,
		Pages:        tview.NewPages(),
		Index:        NewIndexPage(source, msgs),
		Detail:       NewDetailPage(source, msgs),
	}

	app.App.SetRoot(app.Layout, true)

	app.Layout.SetDirection(tview.FlexRow).
		AddItem(app.KeyShortcuts, 1, 1, false).
		AddItem(app.Pages, 0, 1, false).
		AddItem(app.Status, 1, 1, false).
		AddItem(app.Messages.Text, 1, 1, false)

	app.Pages.AddPage(app.Index.GetName(), app.Index.Layout, true, true)
	app.Pages.AddPage(app.Detail.GetName(), app.Detail.Layout, true, false)

	app.Status.SetText(fmt.Sprintf("[yellow:blue]--- Bichon: %s/%s", repo.Server, repo.Project) + strings.Repeat(" ", 500))

	app.App.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Key() == tcell.KeyRune {
			if event.Rune() == 'q' {
				if app.ThisPage.GetName() == app.Detail.GetName() {
					app.switchToPage(app.Index)
				} else {
					app.App.Stop()
				}
			} else if event.Rune() == 'r' {
				go app.refresh()
			}
		} else if event.Key() == tcell.KeyCR {
			if app.ThisPage.GetName() == app.Index.GetName() &&
				app.Index.GetSelectedMergeReq() != nil {
				app.switchToPage(app.Detail)
				app.refresh()
			}
		} else {
			app.Detail.Navigate(event)
		}
		return event
	})

	return app
}

func (app *App) switchToPage(page Page) {
	app.Pages.SwitchToPage(page.GetName())
	app.ThisPage = page
	app.ThisPage.SetFocus(app.App)
	shortcuts := "[yellow:blue]" + app.ThisPage.GetKeyShortcuts() + strings.Repeat(" ", 500)
	app.KeyShortcuts.SetText(shortcuts)
}

func (app *App) refresh() {
	if app.ThisPage.GetName() == app.Detail.GetName() {
		mreq := app.Index.GetSelectedMergeReq()

		go app.Detail.Refresh(app.App, mreq)
	} else {
		go app.Index.Refresh(app.App)
	}
}

func (app *App) Run() {
	app.switchToPage(app.Index)
	app.refresh()
	app.App.Run()
}
