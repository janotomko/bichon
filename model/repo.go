// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"fmt"
	"os"
	"path"
	"strings"

	"github.com/go-ini/ini"
	"github.com/golang/glog"
	"gopkg.in/src-d/go-git.v4/plumbing/transport"
)

type Repo struct {
	Directory string
	Server    string
	Project   string
}

func findGitDir(dir string) (string, error) {
	gitdir := path.Join(dir, ".git")
	_, err := os.Stat(gitdir)
	if err != nil {
		if !os.IsNotExist(err) {
			return "", err
		}
		parent, _ := path.Split(dir)
		return findGitDir(parent)
	}

	return gitdir, nil
}

func findGitRemote(dir string, remoteName string) (string, string, error) {
	gitdir, err := findGitDir(dir)
	if err != nil {
		return "", "", err
	}

	gitcfg := path.Join(gitdir, "config")

	cfg, err := ini.Load(gitcfg)
	if err != nil {
		return "", "", err
	}

	remoteStr := fmt.Sprintf("remote \"%s\"", remoteName);
	remote := cfg.Section(remoteStr).Key("url").String()

	if remote == "" {
		return "", "", fmt.Errorf("No remote '%s' found in %s", remoteName, gitcfg)
	}

	return remote, dir, nil
}

func FindLocalRepo(remoteName string) (*Repo, error) {
	here, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	origin, gitdir, err := findGitRemote(here, remoteName)
	if err != nil {
		return nil, err
	}

	ep, err := transport.NewEndpoint(origin)

	if ep.Host == "" {
		return nil, fmt.Errorf("Expected a hostname in git remote '%s'", origin)
	}

	project := strings.TrimPrefix(strings.TrimSuffix(ep.Path, ".git"), "/")

	repo := &Repo{
		Directory: gitdir,
		Server:    ep.Host,
		Project:   project,
	}

	glog.Infof("Repo '%s' host '%s' project '%s'",
		repo.Directory, repo.Server, repo.Project)
	return repo, nil
}

func (repo *Repo) GetToken() (string, error) {
	home := os.Getenv("HOME")
	file := home + "/.config/bichon/servers.conf"

	cfg, err := ini.Load(file)
	if err != nil {
		return "", err
	}

	token := cfg.Section(repo.Server).Key("token").String()

	glog.Infof("Repo host '%s' token '%s'",
		repo.Server, token)

	if token == "" {
		return "", fmt.Errorf("No authentication in %s for server '%s'",
			file, repo.Server)
	}

	return token, nil
}
