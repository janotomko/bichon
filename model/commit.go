// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"time"
)

type Commit struct {
	Hash          string
	Title         string
	Author        User
	AuthorDate    *time.Time
	Committer     User
	CommitterDate *time.Time
	Message       string
}

func (commit *Commit) Age() string {
	age := time.Since(*commit.AuthorDate)
	return formatDuration(age)
}
