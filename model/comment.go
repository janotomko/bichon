// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"time"
)

type Comment struct {
	Author      User
	Created     *time.Time
	Description string
}
