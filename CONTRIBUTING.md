Contributing to Bichon
======================

Unsurprisingly Bichon accepts contributions via GitLab merge requests
to [the project](https://gitlab.com/bichon-project/bichon/)

An important thing to note, however, is that all contributors must attest
that their submissions satisfy the requirements set out in the [Developer
Certificate of Origin](https://developercertificate.org/)

Contributors attest that they agree with the DCO rules by adding a single
line to the end of their git commit message

```
  Signed-off-by: YOUR NAME <YOUR-EMAIL>
```

This can be facilitated by using the `-s` argument to `git commit`


Git Commit Best Practice
------------------------

Contributors are encouraged to follow a few simple guidelines for code
they submit

 - Each commit should only address one bug or one self contained part
   of a feature. If multiple independant things need changing, then
   create a series of commits and submit the series as a single change
   request

 - Commit messages should describe the rationale/thinking behind a change.
   ie focus on the /why/ behind a change, rather than the /what/

 - Commit messages should be a self contained description. If it is
   fixing a bug describe the problem & the reason for the proposed
   solution directly in the commit message. Don't require the reviewer
   to read the bug report to understand what's wrong.
