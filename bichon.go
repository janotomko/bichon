// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/spf13/pflag"
	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/bichon/source"
	"gitlab.com/bichon-project/bichon/view"
)

type Server struct {
	Hostname string
	Token    string
}

func main() {
	var remoteName string;

	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.CommandLine.StringVar(&remoteName, "remote", "origin", "The remote repo to connect to")
	pflag.Parse()

	repo, err := model.FindLocalRepo(remoteName)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to get git repo info: %s\n", err)
		return
	}

	gl, err := source.NewGitLabForRepo(repo)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to get gitlab source %s: %s\n", repo.Server, err)
		return
	}

	err = gl.Ping()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to validate project %s: %s\n", repo.Project, err)
		return
	}

	app := view.NewApp(repo, gl)

	app.Run()
}
